# Mapping the Global Antibiotics Supply Chain: Interview Task

This repository contains the necessary documents for the tasks to be completed as part of your interview process.

## Repository Structure

- `data.csv`: The raw dataset containing antibiotic resistance information for different regions of Switzerland.

- `interview_exercise.pdf`: A PDF file with the instructions detailing the tasks to be completed.

- `README.md`: This file, providing an overview of the repository contents.

---

Chair of Systems Design
ETH Zurich  
www.sg.ethz.ch
